Pod::Spec.new do |s|


  s.name         = "zonar_NFCScanner_ios"
  s.version      = "1.0.3"
  s.summary      = "zonar_NFCScanner_ios is provide functions to scan NFC tags available in zonar systems."

  s.description  = <<-DESC
"Provides a common library to support scanning NFC tags available in zonar systems"
                DESC

  s.homepage     = "https://gitlab.com/tudoan_GCS/zonarnfc"
  # s.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"


  # ―――  s License  ――――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  Licensing your code is important. See https://choosealicense.com for more info.
  #  CocoaPods will detect a license file if there is a named LICENSE*
  #  Popular ones are 'MIT', 'BSD' and 'Apache License, Version 2.0'.
  #

  s.license      = "MIT"
  # s.license      = { :type => "MIT", :file => "FILE_LICENSE" }

  s.author             = { "Tu Doan" => "c.tu.anh.doan@zonarsystems.com" }
  # Or just: s.author    = "tudoan_GCS"
  # s.authors            = { "tudoan_GCS" => "c.tu.anh.doan@zonarsystems.com" }
  # s.social_media_url   = "https://twitter.com/tudoan_GCS"

  s.source       = { :git => "https://gitlab.com/tudoan_GCS/zonarnfc.git", :tag => "#{s.version}" }
  # s.source       = { :path => './' }

  s.ios.deployment_target   = '11.0'
  s.swift_version           = '5.1'
  # s.source_files            = 'zonar_NFCScanner_ios/**/*'
  s.source_files = "zonar_NFCScanner_ios/**/*.{h,m,swift,strings}"
# s.source_files = "zonar_NFCScanner_ios/*.{h,m,swift,strings}"



  # s.exclude_files = "Classes/Exclude"
  # s.platform     = :ios, "11.0"

  # s.public_header_files = "Classes/**/*.h"

end
