//
//  ViewController.swift
//  ZonarNFCExample
//
//  Created by gcs test on 10/6/20.
//

import UIKit
import zonar_NFCScanner_ios
import CoreNFC

class ViewController: UIViewController {
    let zn = ZNScanner()
    var nfcSession: NFCNDEFReaderSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
    }
    
    
    @IBAction func tapScan(_ sender: Any) {
        //        zn.tap()
        //        self.present(zn.createAlertController(), animated: true, completion: {
        //            print("Scanning complete")
        //        })
        
        self.nfcSession = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        self.nfcSession?.alertMessage = "Hold near to scan"
        self.nfcSession?.begin()
        
        //        self.present(alertController, animated: true, completion: {
        //            print("Scanning complete")
        //        })
    }
    
    private func validateNFCAvailability() {
        guard NFCNDEFReaderSession.readingAvailable else {
            let alertController = UIAlertController(
                title: "Scanning Not Supported",
                message: "This device doesn't support tag scanning.",
                preferredStyle: .alert
            )
            
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
    }
    
}

extension ViewController: NFCNDEFReaderSessionDelegate {
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
//        guard
//            let readerError = error as? NFCReaderError,
//            readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead,
//            readerError.code != .readerSessionInvalidationErrorUserCanceled else {
//            return
//        }
//
//        let alertController = UIAlertController(
//            title: "Session Invalidated",
//            message: error.localizedDescription,
//            preferredStyle: .alert
//        )
//
//        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        guard let alertController = zn.errorAlertController(for: session, error: error) else {
            return
        }
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        var result = ""
        for payload in messages[0].records {
            result += String.init(data: payload.payload.advanced(by: 3), encoding: .utf8)!
        }
        
        print("result = \(result)")
        
        let alertController = UIAlertController(
            title: "NFC Data:",
            message: "result = \(result)",
            preferredStyle: .alert
        )
        
        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: use this then readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) is unavailable
//    @available(iOS 13.0, *)
//    func readerSession(_ session: NFCNDEFReaderSession, didDetect tags: [NFCNDEFTag]) {
//        print("tags = \(tags)")
//    }
    
}
